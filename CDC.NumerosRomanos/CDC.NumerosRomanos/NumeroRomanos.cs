﻿using NUnit.Framework;
using System;

namespace CDC.NumerosRomanos
{
    [TestFixture]
    public class NumeroRomanos
    {
        [Test]
        public void DeveEntederOSimboloI()
        {
            //Arrange
            ConversorDeNumeroRomano convensorRomano = new ConversorDeNumeroRomano();

            //Act
            int numero = convensorRomano.Converte("I");

            //Assert
            Assert.AreEqual(1, numero);

        }
    }

    public class ConversorDeNumeroRomano
    {
        public int Converte(string numero)
        {
            return 0;
        }
    }

}
